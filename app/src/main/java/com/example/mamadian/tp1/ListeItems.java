package com.example.mamadian.tp1;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by mamadian on 07/02/16.
 */
public class ListeItems extends Activity {

    private ArrayList<HashMap<String, String>> listItem = new ArrayList<HashMap<String, String>>();
    private HashMap<String, String> map;
    ListView listview;
    SimpleAdapter adapter;
    EditText edit;
    Button add_user;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.content_nouveauclient);
        listview = (ListView) findViewById(R.id.listView);
        edit = (EditText) findViewById(R.id.typehear);


        // Chercher dans la liste
        edit.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                ListeItems.this.adapter.getFilter().filter(s);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        adapter = new SimpleAdapter(this.getBaseContext(), listItem, R.layout.item,
                new String[]{"img","nom", "prenom"}, new int[]{R.id.img,R.id.itemnom, R.id.itemprenom});
        // Récuperation de données
        recoverData();
        listview.setAdapter(adapter);

        // Bouton ajout
        add_user = (Button)findViewById(R.id.nouveauC);
        add_user.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(), MainActivity.class);
                startActivityForResult(i, 0);
            }
        });

    }

    // Recuperer les données de la base de données
    public void recoverData(){
        String columns[] = new String[] {ContactDB.Name, ContactDB.Prenom,ContactDB.Ville };
        Uri contacts = PersistenceContentProvider.CONTENT_URI;
        Cursor cur = getContentResolver().query(contacts, columns, null, null, null);
        cur.moveToFirst();

        if (cur.moveToFirst()) {
            String name = null;
            do {
                map = new HashMap<String, String>();
                map.put("nom",cur.getString(cur.getColumnIndex(ContactDB.Name)));
                map.put("prenom",cur.getString(cur.getColumnIndex(ContactDB.Prenom)));
                map.put("img", String.valueOf(R.mipmap.ic_launcher));
                listItem.add(map);
            } while (cur.moveToNext());
        }
    }
    // Sauvegarder dans la base de données
    public void saveContact(Model personne){
        ContentValues contact = new ContentValues();
        contact.put(ContactDB.Name, personne.getName());
        contact.put(ContactDB.Prenom, personne.getPrenom());
        contact.put(ContactDB.Ville, personne.getVille());
        this.getContentResolver().insert(PersistenceContentProvider.CONTENT_URI, contact);
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent intent){
        super.onActivityResult(requestCode, resultCode, intent);
        Bundle extras = intent.getExtras();
        if(!(extras==null)){
            Model personne = (Model)extras.getParcelable("personne");
            //Save
            saveContact(personne);
            map = new HashMap<String, String>();
            map.put("nom",personne.getName());
            map.put("prenom",personne.getPrenom());
            map.put("img", String.valueOf(R.mipmap.ic_launcher));
            listItem.add(map);
            adapter.notifyDataSetChanged();
        }

    }

}
