package com.example.mamadian.tp1;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by mamadian on 07/02/16.
 */
public class Model implements Parcelable {

    String name;
    String prenom;
    String ville;
    String date;
    String dpt;

    public Model() {
    }

    public Model(String name, String prenom, String ville, String date, String dpt) {
        this.name = name;
        this.prenom = prenom;
        this.ville = ville;
        this.date = date;
        this.dpt = dpt;
    }

    public Model(Parcel in){

        String[] donnee=new String[5];
        in.readStringArray(donnee);
        this.name=donnee[0];
        this.prenom=donnee[1];
        this.ville=donnee[2];
        this.date=donnee[3];
        this.dpt =donnee[4];
    }
    public String getName() {
        return name;
    }

    public String getPrenom() {
        return prenom;
    }

    public String getVille() {
        return ville;
    }

    public String getDate() {
        return date;
    }

    public String getDpt() {
        return dpt;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public void setVille(String ville) {
        this.ville = ville;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public void setDpt(String dpt) {
        this.dpt = dpt;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeStringArray(new String[] {
                this.name,
                this.prenom,
                this.ville,
                this.date,
                this.dpt
        });
    }

    public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
        public Model createFromParcel(Parcel in) {
            return new Model(in);
        }

        public Model[] newArray(int size) {
            return new Model[size];
        }
    };
}
