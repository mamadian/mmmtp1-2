package com.example.mamadian.tp1;

import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

/**
 * Created by mamadian on 12/02/16.
 */
public class ContactDB {
    public static final String contact_ROWID = "_id";
    public static final String Name = "nom";
    public static final String Prenom = "prenom";
    public static final String Ville = "ville";

    private static final String LOG_TAG = "ContactDb";
    public static final String SQLITE_TABLE = "contact";


    private static final String DATABASE_CREATE =
            "CREATE TABLE " + SQLITE_TABLE + " (" + contact_ROWID + " INTEGER PRIMARY KEY AUTOINCREMENT," + Name + " VARCHAR(255)," + Prenom + " VARCHAR(255)," +  Ville + " VARCHAR(255)" + ");";

    public static void onCreate(SQLiteDatabase db) {
        Log.w(LOG_TAG, DATABASE_CREATE);
        db.execSQL(DATABASE_CREATE);
    }

    public static void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + SQLITE_TABLE);
        onCreate(db);
    }
}
