package com.example.mamadian.tp1;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.support.annotation.Nullable;
import android.util.Log;

import java.util.HashMap;

/**
 * Created by mamadian on 07/02/16.
 */
public class PersistenceContentProvider extends ContentProvider {

    private DataBaseHelper dbHelper;
    private static final String AUTHORITY = "com.example.mamadian.tp1.contact";
    public static final Uri CONTENT_URI = Uri.parse("content://" + AUTHORITY);
    public static final String CONTENT_PROVIDER_MIME = "vnd.android.cursor.item/vnd.tp1.android.content.provider.contact";

    /**
     * On instancie une nouvelle base de données avec pour nom contact et version 1
     * @return
     */
    @Override
    public boolean onCreate() {
        dbHelper = new DataBaseHelper(getContext());
        return false;
    }

    /**
     * cette fonction permet de recuperer l'identifiant du contatc a partir de l'uri
     * @param uri
     * @return
     */
    public long getId(Uri uri){
        String lastPathSegment = uri.getLastPathSegment();
        if (lastPathSegment != null) {
            try {
                return Long.parseLong(lastPathSegment);
            } catch (NumberFormatException e) {
                Log.e("tp1Mamadian", "Number Format Exception : " + e);
            }
        }
        return -1;
    }

    @Nullable
    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        long id = getId(uri);
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        if (id < 0) {
            return  db.query(ContactDB.SQLITE_TABLE,
                    projection, selection, selectionArgs, null, null,
                    sortOrder);
        } else {
            return  db.query(ContactDB.SQLITE_TABLE,
                    projection, ContactDB.contact_ROWID + "=" + id, null, null, null,
                    null);
        }

    }

    @Nullable
    @Override
    public String getType(Uri uri) {
        return CONTENT_PROVIDER_MIME;
    }

    @Nullable
    @Override
    public Uri insert(Uri uri, ContentValues values) {
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        try {
            long id = db.insertOrThrow(ContactDB.SQLITE_TABLE, null, values);

            if (id == -1) {
                throw new RuntimeException(String.format(
                        "%s : Failed to insert [%s] for unknown reasons.","TP1MamadianAndroid", values, uri));
            } else {
                return ContentUris.withAppendedId(uri, id);
            }

        } finally {
            db.close();
        }

    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        long id = getId(uri);
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        try {
            if (id < 0)
                return db.delete(
                        ContactDB.SQLITE_TABLE,
                        selection, selectionArgs);
            else
                return db.delete(
                        ContactDB.SQLITE_TABLE,
                        ContactDB.contact_ROWID+ "=" + id, selectionArgs);
        } finally {
            db.close();
        }
    }

    @Override
    public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        try {
           int nbRow= db.update(ContactDB.SQLITE_TABLE,values,selection,selectionArgs);
            if(nbRow<=0){
                throw new RuntimeException(String.format(
                        "%s : Failed to update [%s] for unknown reasons.","TP1MamadianAndroid", values, uri));
            }
            else{
                return nbRow;
            }

        }
        finally {
            db.close();
        }

    }
}
